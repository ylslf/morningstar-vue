module.exports = {
  tabWidth: 2,
  semi: true,
  printWidth: 140,
  singleQuote: true,
  quoteProps: 'consistent',
  htmlWhitespaceSensitivity: 'strict',
  vueIndentScriptAndStyle: true,
};
