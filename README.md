# morningstar-vue

#### 介绍

基于ARCO DESIGN实现的后台管理框架  
目前实现功能

- 基础信息管理功能
  ![](readme/系统管理-2.jpeg)
  ![](readme/系统管理-1.jpeg)
- 可视化表单
  ![](readme/表单设计-列表.jpeg)
  ![](readme/表单设计-设计器.jpeg)
  ![](readme/表单设计-表单权限.jpeg)
  ![](readme/表单设计-表单功能.jpeg)
- 流程审批
  ![](readme/流程审批-列表.png)
  ![](readme/流程审批-流程设计器.png)
  ![](readme/流程审批-节点设置-审批任务.png)
  ![](readme/流程审批-节点设置-抄送.png)
  ![](readme/流程审批-节点设置-条件.png)
  ![](readme/流程审批-发起流程.png)
  ![](readme/流程审批-审批任务.png)
  ![](readme/流程审批-我的任务列表.png)

- 完善登录逻辑和基础信息模块
- 表单组件-可编辑表格
- 表单基础数据管理 select 等组件从后台获取数据
- 表单组件事件配置

#### 软件架构

软件架构说明

#### 安装教程

```
npm install
npm run dev
```

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
