import { mergeConfig } from 'vite';
import eslint from 'vite-plugin-eslint';
import baseConfig from './vite.config.base';

export default mergeConfig(
  {
    mode: 'development',
    server: {
      port: '8081',
      proxy: {
        '/service-authorization': {
          target: 'http://localhost:5000/',
          changeOrigin: true
        },
        '/service-system': {
          target: 'http://localhost:5000/',
          changeOrigin: true
        },
        '/service-flow': {
          target: 'http://localhost:5000/',
          changeOrigin: true
        }
      }
    },
    plugins: [
      eslint({
        cache: false,
        include: ['src/**/*.ts', 'src/**/*.tsx', 'src/**/*.vue'],
        exclude: ['node_modules']
      })
    ]
  },
  baseConfig
);
