import { FileItem } from '@arco-design/web-vue/es/upload/interfaces';

export interface UserState {
  userId?: string;
  loginName?: string;
  userName?: string;
  email?: string;
  phone?: string;
  sex?: string;
  avatar?: string;
  deptName?: string;
  roleNames?: Array<String>;
  postNames?: Array<String>;
  permissions?: Array<String>;
  loginIp?: string;
  loginDate?: string;
  createTime?: string;
  arcoDesignFiles?: FileItem[];
}
