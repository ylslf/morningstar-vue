import { createRouter, createWebHistory } from "vue-router";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css";
import appRoutes from "./routes/index";
import { REDIRECT_MAIN } from "./routes/base";
import createRouteGuard from "./guard";

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const router = createRouter({
  history: createWebHistory(),
  strict: true,
  routes: [
    {
      path: "/",
      redirect: "login"
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/login/index.vue"),
      meta: {
        requiresAuth: false
      }
    },
    ...appRoutes,
    REDIRECT_MAIN
  ],
  scrollBehavior() {
    return { top: 0 };
  }
});

createRouteGuard(router);

export default router;
