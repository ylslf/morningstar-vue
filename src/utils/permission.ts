import { useUserStore } from '@/store';

/**
 * 校验是否有权限
 * @param permissionValues
 */
export const hasPermission = (permissionValues: any) => {
  const userStore = useUserStore();
  const { permissions } = userStore;
  if (Array.isArray(permissionValues)) {
    if (permissionValues.length > 0) {
      const permissionValuesSet = new Set(permissionValues);
      // @ts-ignore
      return permissions.some(item => permissionValuesSet.has(item));
    }
  }
  return false;
};