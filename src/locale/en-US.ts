import localeMessageBox from '@/components/message-box/locale/en-US';
import localeLogin from '@/views/login/locale/en-US';

import localeWorkplace from '@/views/dashboard/workplace/locale/en-US';

import localeUserSetting from '@/views/user/setting/locale/en-US';


import localeSettings from './en-US/settings';
import localeTable from './en-US/table';
import localeMenu from './en-US/menu';

export default {
  ...localeSettings,
  ...localeMessageBox,
  ...localeLogin,
  ...localeWorkplace,
  ...localeUserSetting,
  ...localeTable,
  ...localeMenu
};
