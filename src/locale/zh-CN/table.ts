export default {

  'table.query.search': '查询',
  'table.query.reset': '重置',

  'table.operator.add': '新增',
  'table.operator.read.all': '一键已读',
  // size
  'table.size.mini': '迷你',
  'table.size.small': '偏小',
  'table.size.medium': '中等',
  'table.size.large': '偏大',

  // actions
  'table.actions.refresh': '刷新',
  'table.actions.density': '密度',
  'table.actions.columnSetting': '列设置',

  'table.row.operator.detail': '详情'
};
