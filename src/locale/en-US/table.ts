export default {

  'table.query.search':'search',
  'table.query.reset':'reset',

  'table.operator.add': 'add',
  'table.operator.read.all': 'ReadAll',
  // size
  'table.size.mini': 'mini',
  'table.size.small': 'small',
  'table.size.medium': 'middle',
  'table.size.large': 'large',
  // actions
  'table.actions.refresh': 'refresh',
  'table.actions.density': 'density',
  'table.actions.columnSetting': 'columnSetting',

  'table.row.operator.detail': 'detail',
};
