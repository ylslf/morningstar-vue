import localeMessageBox from '@/components/message-box/locale/zh-CN';
import localeLogin from '@/views/login/locale/zh-CN';

import localeWorkplace from '@/views/dashboard/workplace/locale/zh-CN';

import localeUserSetting from '@/views/user/setting/locale/zh-CN';


import localeSettings from './zh-CN/settings';
import localeTable from './zh-CN/table';
import localeMenu from './zh-CN/menu';

export default {
  ...localeSettings,
  ...localeMessageBox,
  ...localeLogin,
  ...localeWorkplace,

  ...localeUserSetting,
  ...localeTable,
  ...localeMenu
};
