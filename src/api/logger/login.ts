import axios from 'axios';

export interface SysLogLogin {
  logId: string;
  loginName: string;
  loginIp: string;
  loginLocation: string;
  browser: string;
  os: string;
  status: string;
  msg: string;
  loginTime: string;
}

/**
 * 系统配置列表
 * @param data
 */
export function loggerLoginList(data: any) {
  return axios.post('/service-system/logger/login/list', data);
}