import axios from 'axios';

export interface SysLogOperate {
  logId: string;
  title: string;
  businessType: string;
  method: string;
  requestMethod: string;
  operatorType: string;
  operateId: string;
  operateUrl: string;
  operateIp: string;
  browser: string;
  os: string;
  status: string;
  resultMsg: string;
  operateTime: string;
  costTime: string;
  operateParam: string;
  resultData: string;
  operateName: string;
}

/**
 * 操作日志 列表
 * @param data
 */
export function loggerOperateList(data: any) {
  return axios.post('/service-system/logger/operate/list', data);
}

/**
 * 操作日志详细
 * @param id
 */
export function loggerOperateDetail(id: string) {
  return axios.get(`/service-system/logger/operate/detail?logId=${id}`);
}