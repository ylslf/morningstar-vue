import axios from 'axios';

export interface SysMessage {
  messageId: string,
  userId: string,
  messageTitle: string,
  messageType: string,
  messageContent: string,
  status: string,
  remark: string,
  createTime: string
}

/**
 * 我的消息列表
 * @param data
 */
export function messageList(data: any) {
  return axios.post('/service-system/system/message/list', data);
}

export function unReadMessage() {
  return axios.post('/service-system/system/message/unReadMessage');
}

export function readMessage(messageId: string) {
  return axios.get(`/service-system/system/message/read?messageId=${messageId}`);
}

export function readAllMessage() {
  return axios.get('/service-system/system/message/readAll');
}

export function deleteMessage(messageId: string) {
  return axios.get(`/service-system/system/message/delete?messageId=${messageId}`);
}