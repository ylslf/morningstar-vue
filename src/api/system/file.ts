import axios from 'axios';

export const FILE_UPLOAD_URL = '/service-system/system/file/upload';

export interface SysFile {
  fileId: string,
  fileName: string;
  fileType: string;
  fileSizeShow: string;
  filePath: any;
  createTime: string;
  createByName: string;
  fileBizNames: Array<string>;
  previewUrl: string;
}

/**
 * 表单列表
 * @param data
 */
export function fileList(data: any) {
  return axios.post('/service-system/system/file/list', data);
}

/**
 * 删除文件
 * @param fileId
 */
export function deleteFile(fileId: string) {
  return axios.get(`/service-system/system/file/delete?fileId=${fileId}`);
}


/**
 * 表单详细
 * @param fileId
 */
export function detailFile(fileId: string) {
  return axios.get<SysFile>(`/service-system/system/file/detail?fileId=${fileId}`);
}

export function downLoad(fileId: string, fileName: string) {
  axios.get(
    `/service-system/system/file/download?fileId=${fileId}`,
    {
      responseType: 'blob'
    }
  ).then(res => {
    // @ts-ignore
    const blob = new Blob([res],{type: 'application/octet-stream'});
    const link = document.createElement('a');
    link.download = fileName;
    link.style.display = 'none';
    link.href = URL.createObjectURL(blob);
    document.body.appendChild(link);
    link.click();
    URL.revokeObjectURL(link.href); // 释放URL 对象
    document.body.removeChild(link);
  });
}