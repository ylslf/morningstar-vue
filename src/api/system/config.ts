import axios from 'axios';

export interface SysConfig {
  configId?: string;
  configName?: string;
  configKey?: string;
  configValue?: any;
  configType?: string;
  remark?: string;
}

/**
 * 系统配置列表
 * @param data
 */
export function configList(data: any) {
  return axios.post('/service-system/system/config/list', data);
}

/**
 * 新增系统配置
 * @param data
 */
export function addConfig(data: SysConfig) {
  return axios.post('/service-system/system/config', data);
}

/**
 * 更新系统配置
 * @param data
 */
export function updateConfig(data: SysConfig) {
  return axios.post('/service-system/system/config/update', data);
}

/**
 * 删除系统配置
 * @param configId
 */
export function deleteConfig(configId: string, configKey: string) {
  return axios.get(`/service-system/system/config/delete?configId=${configId}&configKey=${configKey}`);
}

/**
 * 系统配置详细
 * @param configId
 */
export function detailConfig(configId: string) {
  return axios.get<SysConfig>(`/service-system/system/config/detail?configId=${configId}`);
}