import axios from 'axios';

export interface SysMessageTemplate {
  templateId?: string;
  templateName?: string;
  templateKey?: string;
  templateMessage?: any;
  templateEmail?: string;
  sendType?: string;
  status?: string;
  remark?: string;
}

/**
 * 消息模板列表
 * @param data
 */
export function templateList(data: any) {
  return axios.post('/service-system/system/message/template/list', data);
}

/**
 * 新增消息模板
 * @param data
 */
export function addTemplate(data: SysMessageTemplate) {
  return axios.post('/service-system/system/message/template/add', data);
}

/**
 * 更新消息模板
 * @param data
 */
export function updateTemplate(data: SysMessageTemplate) {
  return axios.post('/service-system/system/message/template/update', data);
}

/**
 * 删除消息模板
 * @param templateId
 */
export function deleteTemplate(templateId: string) {
  return axios.get(`/service-system/system/message/template/delete?templateId=${templateId}`);
}

/**
 * 消息模板详细
 * @param templateId
 */
export function detailTemplate(templateId: string) {
  return axios.get<SysMessageTemplate>(`/service-system/system/message/template/detail?templateId=${templateId}`);
}