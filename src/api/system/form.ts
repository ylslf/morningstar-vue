import axios from 'axios';
import { SelectOption } from '@arco-design/web-vue';

export interface SysFrom {
  formId?: string;
  formName?: string;
  formConfig?: any;
  formList?: any;
  formModel?: any;
  formItem?: any;
  formPermission?: any;
  status?: string;
  remark?: string;
  eventConfigs?: any;
}

/**
 * 表单列表
 * @param data
 */
export function formList(data: any) {
  return axios.post('/service-system/system/form/list', data);
}

/**
 * 新增表单
 * @param data
 */
export function addForm(data: SysFrom) {
  return axios.post('/service-system/system/form', data);
}

/**
 * 更新表单
 * @param data
 */
export function updateForm(data: SysFrom) {
  return axios.post('/service-system/system/form/update', data);
}

/**
 * 删除表单
 * @param formId
 */
export function deleteForm(formId: string) {
  return axios.get(`/service-system/system/form/delete?formId=${formId}`);
}

/**
 * 表单详细
 * @param formId
 */
export function detailForm(formId: string) {
  return axios.get<SysFrom>(`/service-system/system/form/detail?formId=${formId}`);
}

/**
 * 下拉选
 */
export function selectForm() {
  return axios.get<SelectOption[]>('/service-system/system/form/select');
}