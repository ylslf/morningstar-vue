import axios from 'axios';
import type { RouteRecordNormalized } from 'vue-router';
import { UserState } from '@/store/modules/user/types';

export interface LoginData {
  name: string;
  pwd: string;
}

export interface LoginRes {
  tokenName: string;
  tokenValue: string;
  isLogin: boolean;
  loginId: string;
  loginType: string;
  tokenTimeout: string;
  sessionTimeout: string;
  tokenSessionTimeout: string;
  tokenActivityTimeout: string;
  loginDevice: string;
  tag: string;
}

export interface SysUser {
  userId?: string;
  loginName?: string;
  userName?: string;
  userType?: string;
  email?: string;
  phone?: string;
  sex?: string;
  status?: string;
  deptId?: string;
  avatar?: string;
  roleIds?: string[];
  postIds?: string[];
  createTime?: string;
}

export interface UpdatePassword {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}

/**
 * 用户登录
 * @param data
 */
export function login(data: LoginData) {
  return axios.post<LoginRes>('/service-authorization/auth/login', data);
}

/**
 * 用户登出
 */
export function logout() {
  return axios.post<LoginRes>('/service-authorization/auth/logout');
}

/**
 * 获取用户信息
 */
export function getUserInfo() {
  return axios.get<UserState>('/service-system/system/user/getUserInfo');
}

/**
 * 获取用户菜单
 */
export function getMenuList() {
  return axios.get<RouteRecordNormalized[]>('/service-system/system/menu/userMenu');
}

/**
 * 用户列表
 * @param data
 */
export function userList(data: any) {
  return axios.post('/service-system/system/user/list', data);
}

/**
 * 新增用户
 * @param data
 */
export function addUser(data: SysUser) {
  return axios.post('/service-system/system/user', data);
}

/**
 * 更新用户
 * @param data
 */
export function updateUser(data: SysUser) {
  return axios.post('/service-system/system/user/update', data);
}

/**
 * 删除用户
 * @param userId
 */
export function deleteUser(userId: string) {
  return axios.get(`/service-system/system/user/delete?userId=${userId}`);
}

/**
 * 详细信息
 * @param userId
 */
export function detailUser(userId: string) {
  return axios.get<SysUser>(`/service-system/system/user/detail?userId=${userId}`);
}

/**
 * 选择用户列表
 *
 */
export function selectUser() {
  return axios.get<SysUser[]>('/service-system/system/user/select');
}

/**
 * 上传用户头像
 * @param avatar 头像
 */
export function updateAvatar(avatar: string) {
  return axios.get<SysUser[]>(`/service-system/system/user/updateAvatar?avatar=${avatar}`);
}

/**
 * 更新用户信息
 * @param data  用户信息
 */
export function updateUserInfo(data: SysUser) {
  return axios.post('/service-system/system/user/updateUserInfo', data);
}

/**
 * 更新用户密码
 * @param data
 */
export function updatePassword(data: UpdatePassword) {
  return axios.post('/service-system/system/user/updatePassword', data);
}

/**
 * 重置密码
 * @param userId
 */
export function resetPassword(userId: string) {
  return axios.get(`/service-system/system/user/resetPassword/${userId}`);
}