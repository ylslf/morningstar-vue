import axios from 'axios';

export interface SysDictData {
  dictCode?: string;
  dictType?: string;
  dictLabel?: string;
  dictValue?: string;
  dictSort?: any;
  cssClass?: string;
  listClass?: string;
  isDefault?: string;
  status?: string;
  remark?: string;
}

/**
 * 数据字典数据列表
 * @param data
 */
export function dictDataList(data: any) {
  return axios.post('/service-system/system/dict/data/list', data);
}

/**
 * 新增数据字典数据
 * @param data
 */
export function addDictData(data: SysDictData) {
  return axios.post('/service-system/system/dict/data', data);
}

/**
 * 更新数据字典数据
 * @param data
 */
export function updateDictData(data: SysDictData) {
  return axios.post('/service-system/system/dict/data/update', data);
}

/**
 * 删除数据字典数据
 * @param dictCode
 */
export function deleteDictData(dictCode: string) {
  return axios.get(`/service-system/system/dict/data/delete?dictCode=${dictCode}`);
}

/**
 * 数据字典类型数据
 * @param dictCode
 */
export function detailDictData(dictCode: string) {
  return axios.get<SysDictData>(`/service-system/system/dict/data/detail?dictCode=${dictCode}`);
}
