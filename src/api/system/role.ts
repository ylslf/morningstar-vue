import axios from 'axios';

export interface SysRole {
  roleId?: string;
  roleName?: string;
  roleKey?: string;
  roleSort?: any;
  dataScope?: string;
  status?: string;
  remark?: string;
}

/**
 * 角色列表
 * @param data
 */
export function roleList(data: any) {
  return axios.post('/service-system/system/role/list', data);
}

/**
 * 新增角色
 * @param data
 */
export function addRole(data: SysRole) {
  return axios.post('/service-system/system/role', data);
}

/**
 * 更新角色
 * @param data
 */
export function updateRole(data: SysRole) {
  return axios.post('/service-system/system/role/update', data);
}

/**
 * 删除角色
 * @param roleId
 */
export function deleteRole(roleId: string) {
  return axios.get(`/service-system/system/role/delete?roleId=${roleId}`);
}

/**
 * 角色详细
 * @param roleId
 */
export function detailRole(roleId: string) {
  return axios.get<SysRole>(`/service-system/system/role/detail?roleId=${roleId}`);
}

/**
 * 获取角色权限
 * @param roleId
 */
export function rolePermission(roleId: any) {
  return axios.get<[string]>(`/service-system/system/role/permission?roleId=${roleId}`);
}

export function setPermission(data: any) {
  return axios.post('/service-system/system/role/permission', data);
}

export function roleSelect() {
  return axios.get<SysRole[]>('/service-system/system/role/select');
}