import axios from 'axios';

export interface SysPost {
  postId?: string;
  postCode?: string;
  postName?: string;
  postSort?: any;
  status?: string;
  remark?: string;
}

/**
 * 职位列表
 * @param data
 */
export function postList(data: any) {
  return axios.post('/service-system/system/post/list', data);
}

/**
 * 新增职位
 * @param data
 */
export function addPost(data: SysPost) {
  return axios.post('/service-system/system/post', data);
}

/**
 * 更新职位
 * @param data
 */
export function updatePost(data: SysPost) {
  return axios.post('/service-system/system/post/update', data);
}

/**
 * 删除职位
 * @param postId
 */
export function deletePost(postId: string) {
  return axios.get(`/service-system/system/post/delete?postId=${postId}`);
}

/**
 * 职位详细
 * @param postId
 */
export function detailPost(postId: string) {
  return axios.get<SysPost>(`/service-system/system/post/detail?postId=${postId}`);
}

export function postSelect() {
  return axios.get('/service-system/system/post/select');
}