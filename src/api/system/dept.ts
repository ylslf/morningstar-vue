import axios from 'axios';
import { SysMenu } from '@/api/system/menu';

export interface SysDept {
  deptId?: string;
  deptName?: string;
  parentId?: string;
  leader?: string;
  phone?: string;
  email?: string;
  orderNum?: any;
  status?: string;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
  children?: SysDept[];
}

/**
 * 获取部门
 */
export function deptTree(hasRoot: boolean) {
  return axios.get(`/service-system/system/dept/tree?hasRoot=${hasRoot}`);
}

/**
 * 查询部门列表
 * @param data
 */
export function deptList(data: any) {
  return axios.post('/service-system/system/dept/list', data);
}

/**
 * 添加部门
 * @param data
 */
export function addDept(data: SysDept) {
  return axios.post('/service-system/system/dept', data);
}

/**
 * 更新部门
 * @param data
 */
export function updateDept(data: SysDept) {
  return axios.post('/service-system/system/dept/update', data);
}

/**
 * 删除部门
 * @param deptId
 */
export function deleteDept(deptId: string) {
  return axios.get(`/service-system/system/dept/delete?deptId=${deptId}`);
}

/**
 * 详细信息
 * @param deptId
 */
export function detailDept(deptId: string) {
  return axios.get<SysMenu>(`/service-system/system/dept/detail?deptId=${deptId}`);
}