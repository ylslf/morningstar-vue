import axios from 'axios';

export interface SysMenu {
  menuId?: string,
  menuName?: string,
  parentId?: string,
  orderNum?: any,
  menuType?: string,
  visible?: string,
  isRefresh?: string,
  perms?: string,
  status?: string,
  icon?: string,
  remark?: string,
  routePath?: string,
  routeName?: string,
  routeProps?: string,
  routeComponent?: string,
  createBy?: string,
  createTime?: string,
  updateBy?: string,
  updateTime?: string,
  children?: SysMenu[]
}

/**
 * 获取菜单树  不包含权限
 */
export function menuTree() {
  return axios.get('/service-system/system/menu/tree');
}

/**
 * 获取菜单树  包含权限
 */
export function menuTreeSelect() {
  return axios.get('/service-system/system/menu/select');
}

/**
 * 查询菜单列表
 * @param data
 */
export function menuList(data: any) {
  return axios.post('/service-system/system/menu/list', data);
}

/**
 * 添加菜单
 * @param data
 */
export function addMenu(data: SysMenu) {
  return axios.post('/service-system/system/menu', data);
}

/**
 * 更新菜单
 * @param data
 */
export function updateMenu(data: SysMenu) {
  return axios.post('/service-system/system/menu/update', data);
}

/**
 * 删除菜单
 * @param menuId
 */
export function deleteMenu(menuId: string) {
  return axios.get(`/service-system/system/menu/delete?menuId=${menuId}`);
}

/**
 * 详细信息
 * @param menuId
 */
export function detailMenu(menuId: string) {
  return axios.get<SysMenu>(`/service-system/system/menu/detail?menuId=${menuId}`);
}