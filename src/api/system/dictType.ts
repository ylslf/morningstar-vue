import axios from 'axios';

export interface SysDictType {
  dictId?: string;
  dictName?: string;
  dictType?: string;
  status?: any;
  remark?: string;
}

/**
 * 数据字典类型列表
 * @param data
 */
export function dictTypeList(data: any) {
  return axios.post('/service-system/system/dict/type/list', data);
}

/**
 * 新增数据字典类型
 * @param data
 */
export function addDictType(data: SysDictType) {
  return axios.post('/service-system/system/dict/type', data);
}

/**
 * 更新数据字典类型
 * @param data
 */
export function updateDictType(data: SysDictType) {
  return axios.post('/service-system/system/dict/type/update', data);
}

/**
 * 删除数据字典类型
 * @param dictId
 */
export function deleteDictType(dictId: string) {
  return axios.get(`/service-system/system/dict/type/delete?dictId=${dictId}`);
}

/**
 * 数据字典类型详细
 * @param dictId
 */
export function detailDictType(dictId: string) {
  return axios.get<SysDictType>(`/service-system/system/dict/type/detail?dictId=${dictId}`);
}
