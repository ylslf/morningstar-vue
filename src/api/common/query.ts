/**
 * 列表查询条件 页码
 */
export interface Page {
  current: number;
  size: number;
}

/**
 * 列表查询条件
 */
export interface Query<T> {
  page: Page;
  query: T;
}