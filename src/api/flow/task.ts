import axios from 'axios';

/**
 * 我的代办
 * @param data
 */
export function myTodo(data: any) {
  return axios.post('/service-flow/flow/task/myTodo', data);
}

/**
 * 我的已办
 * @param data
 */
export function myDone(data: any) {
  return axios.post('/service-flow/flow/task/myDone', data);
}

/**
 * 任务详细
 * @param taskId  任务Id
 */
export function detailTask(taskId: string) {
  return axios.get(`/service-flow/flow/task/detail/${taskId}`);
}

/**
 * 完成任务
 * @param data
 */
export function completeTask(data: any) {
  return axios.post('/service-flow/flow/task/complete', data);
}