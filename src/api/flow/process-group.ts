import axios from 'axios';
import { SelectOption } from '@arco-design/web-vue';
import { FlowProcess } from '@/api/flow/process';

export interface FlowProcessGroup {
  processGroupId?: string,
  processGroupName?: string,
  sort?: number,
  processes?: FlowProcess[];
}

/**
 * 流程组列表
 * @param data
 */
export function processGroupList(data: any) {
  return axios.post('/service-flow/flow/process/group/list', data);
}

/**
 * 新增流程组
 * @param data
 */
export function addProcessGroup(data: FlowProcessGroup) {
  return axios.post('/service-flow/flow/process/group', data);
}

/**
 * 更新流程组
 * @param data
 */
export function updateProcessGroup(data: FlowProcessGroup) {
  return axios.post('/service-flow/flow/process/group/update', data);
}

/**
 * 删除流程组
 * @param processGroupId
 */
export function deleteProcessGroup(processGroupId: string) {
  return axios.get(`/service-flow/flow/process/group/delete?processGroupId=${processGroupId}`);
}

/**
 * 流程组详细
 * @param processGroupId
 */
export function detailProcessGroup(processGroupId: string) {
  return axios.get<FlowProcessGroup>(`/service-flow/flow/process/group/detail?processGroupId=${processGroupId}`);
}

/**
 * 获取下拉选
 */
export function selectProcessGroup() {
  return axios.get<SelectOption[]>('/service-flow/flow/process/group/select');
}