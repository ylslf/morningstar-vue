import axios from 'axios';

export interface StartProcessEntity {
  instanceId?: string,
  processId?: string,
  processKey?: string;
  formModel?: any;
}

export interface FlowInstance {
  instanceId: string;
  processId: string;
  processInstanceId: string;
  bizNumber: string;
  formModel: any;
  startUserId: any;
  status: string;
  processName: string;
  startUserName: string;
  createTime: string;
  endTime: string;
  flowTaskList: Array<any>;
  flowProcess: any;
}


/**
 * 我发起的流程列表
 * @param data
 */
export function myStart(data: any) {
  return axios.post('/service-flow/flow/instance/myStart', data);
}

/**
 * 抄送我的流程
 * @param data
 */
export function myCopy(data: any) {
  return axios.post('/service-flow/flow/instance/myCopy', data);
}

/**
 * 查询流程实列
 * @param data
 */
export function list(data: any) {
  return axios.post('/service-flow/flow/instance/list', data);
}

/**
 * 创建草稿
 * @param data
 */
export function draftFlow(data: StartProcessEntity) {
  return axios.post('/service-flow/flow/instance/draft', data);
}

/**
 * 发起流程
 */
export function startFlow(data: StartProcessEntity) {
  return axios.post('/service-flow/flow/instance/start', data);
}

/**
 * 流程实列详细信息
 */
export function detailInstance(instanceId: string) {
  return axios.get<FlowInstance>(`/service-flow/flow/instance/detail/${instanceId}`);
}

/**
 * 删除流程实列
 * @param instanceId
 */
export function deleteInstance(instanceId: string) {
  return axios.get(`/service-flow/flow/instance/delete?instanceId=${instanceId}`);
}

/**
 * 撤销流程
 * @param processInstanceId
 */
export function revocationInstance(processInstanceId: string) {
  return axios.get(`/service-flow/flow/instance/revocation?processInstanceId=${processInstanceId}`);
}