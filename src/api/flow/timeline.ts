import axios from 'axios';

/**
 * 流程实列详细信息
 */
export function history(instanceId: string) {
  return axios.get(`/service-flow/flow/instance/timeline/${instanceId}`);
}

/**
 * 获取流程 节点执行情况
 */
export function flowGraph(instanceId: string) {
  return axios.get(`/service-flow/flow/instance/timeline/flowGraph/${instanceId}`);
}