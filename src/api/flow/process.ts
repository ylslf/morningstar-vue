import axios from 'axios';

export interface FlowProcess {
  processId?: string;
  processName?: string;
  processKey?: string;
  processVersion?: string;
  processGroupId?: string;
  processAdmin?: string,
  formId?: string;
  deploy?: string;
  status?: string;
  processGroupName?: string;
  formName?: string;
  processData?: any;
  nodeList?: [];
}

/**
 * 发起流程列表
 */
export function processGroupList() {
  return axios.post('/service-flow/flow/process/listGroup');
}

/**
 * 流程列表
 * @param data
 */
export function processList(data: any) {
  return axios.post('/service-flow/flow/process/list', data);
}

/**
 * 新增流程
 * @param data
 */
export function addProcess(data: FlowProcess) {
  return axios.post('/service-flow/flow/process', data);
}

/**
 * 更新流程
 * @param data
 */
export function updateProcess(data: FlowProcess) {
  return axios.post('/service-flow/flow/process/update', data);
}

/**
 * 删除流程
 * @param processId
 */
export function deleteProcess(processId: string) {
  return axios.get(`/service-flow/flow/process/delete?processId=${processId}`);
}

/**
 * 流程详细
 * @param processId
 */
export function detailProcess(processId: string) {
  return axios.get<FlowProcess>(`/service-flow/flow/process/detail?processId=${processId}`);
}


/**
 * 部署流程图
 * @param data
 */
export function deployProcess(data: FlowProcess) {
  return axios.post('/service-flow/flow/process/deploy', data);
}

/**
 * 更新流程状态
 * @param data
 */
export function updateStatus(data: FlowProcess) {
  return axios.post('/service-flow/flow/process/updateStatus', data);
}

/**
 * 测试流程条件
 * @param data
 */
export function conditionTestProcess(data: any) {
  return axios.post('/service-flow/flow/process/conditionTest', data);
}