import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeParallelGateway from '@/components/bpmn-design/nodes/node-parallel-gateway/node-parallel-gateway.vue';

export const NODE_PARALLEL_GATEWAY_SHAPE = 'node_parallel_gateway';

export const NODE_PARALLEL_GATEWAY = {
  shape: NODE_PARALLEL_GATEWAY_SHAPE,
  label: '并行网关',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
      {
        id: 'port_right',
        group: 'right'
      },
      {
        id: 'port_top',
        group: 'top'
      },
      {
        id: 'port_bottom',
        group: 'bottom'
      }
    ]
  }
};

export const NODE_PARALLEL_GATEWAY_DEFINE = {
  shape: NODE_PARALLEL_GATEWAY_SHAPE,
  width: 40,
  height: 40,
  ports: PORTS,
  component: NodeParallelGateway,
  data: {
    label: '并行网关'
  }
};