import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeIncludeGateway from '@/components/bpmn-design/nodes/node-include-gateway/node-include-gateway.vue';

export const NODE_INCLUDE_GATEWAY_SHAPE = 'node_include_gateway';

export const NODE_INCLUDE_GATEWAY = {
  shape: NODE_INCLUDE_GATEWAY_SHAPE,
  label: '包容网关',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
      {
        id: 'port_right',
        group: 'right'
      },
      {
        id: 'port_top',
        group: 'top'
      },
      {
        id: 'port_bottom',
        group: 'bottom'
      }
    ]
  }
};

export const NODE_INCLUDE_GATEWAY_DEFINE = {
  shape: NODE_INCLUDE_GATEWAY_SHAPE,
  width: 40,
  height: 40,
  ports: PORTS,
  component: NodeIncludeGateway,
  data: {
    label: '包容网关'
  }
};