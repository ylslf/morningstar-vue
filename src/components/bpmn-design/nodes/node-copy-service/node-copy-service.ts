import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeCopyService from '@/components/bpmn-design/nodes/node-copy-service/node-copy-service.vue';

export const NODE_COPY_SERVICE_SHAPE = 'node_copy_service';

export const NODE_COPY_SERVICE = {
  shape: NODE_COPY_SERVICE_SHAPE,
  label: '抄送节点',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
      {
        id: 'port_right',
        group: 'right'
      },
      {
        id: 'port_top',
        group: 'top'
      },
      {
        id: 'port_bottom',
        group: 'bottom'
      }
    ]
  }
};

export const NODE_COPY_SERVICE_DEFINE = {
  shape: NODE_COPY_SERVICE_SHAPE,
  width: 150,
  height: 50,
  component: NodeCopyService,
  ports: PORTS,
  data: {
    label: '抄送节点',
    copyTarget: {
      users: [],
      roles: [],
      posts: [],
      depts: [],
      deptLeaders: []
    },
    formPermission: {}
  }
};
