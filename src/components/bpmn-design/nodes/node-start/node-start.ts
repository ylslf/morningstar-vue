import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeStart from '@/components/bpmn-design/nodes/node-start/node-start.vue';

export const NODE_START_SHAPE = 'node_start';

export const NODE_START = {
  shape: NODE_START_SHAPE,
  label: '开始',
  ports: {
    items: [
      {
        id: 'port_right',
        group: 'right'
      }
    ]
  }
};

export const NODE_START_DEFINE = {
  shape: NODE_START_SHAPE,
  width: 40,
  height: 40,
  ports: PORTS,
  component: NodeStart,
  data: {
    label: '开始',
    formPermission: {}
  }
};
