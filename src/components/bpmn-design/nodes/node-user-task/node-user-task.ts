import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeUserTask from '@/components/bpmn-design/nodes/node-user-task/node-user-task.vue';

export const NODE_USER_TASK_SHAPE = 'node_user_task';

export const NODE_USER_TASK = {
  shape: NODE_USER_TASK_SHAPE,
  label: '审批节点',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
      {
        id: 'port_right',
        group: 'right'
      },
      {
        id: 'port_top',
        group: 'top'
      },
      {
        id: 'port_bottom',
        group: 'bottom'
      }
    ]
  }
};

export const NODE_USER_TASK_DEFINE = {
  shape: NODE_USER_TASK_SHAPE,
  width: 150,
  height: 50,
  component: NodeUserTask,
  ports: PORTS,
  data: {
    label: '审批节点',
    assign: {
      assignStrategy: undefined,
      assignValue: [],
      assignMultiStrategy: undefined,
      assignMultiPercentage: 100,
      assignEmpty: undefined,
      assignEmptyUser: undefined,
      assignRefuse: undefined
    },
    operate: ['AGREE', 'REFUSE', 'TURN_TO'],
    formPermission: {}
  }
};
