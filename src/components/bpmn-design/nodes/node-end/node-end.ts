import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeEnd from '@/components/bpmn-design/nodes/node-end/node-end.vue';

export const NODE_END_SHAPE = 'node_end';


export const NODE_END = {
  shape: NODE_END_SHAPE,
  label: '结束',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
    ]
  }
};

export const NODE_END_DEFINE = {
  shape: NODE_END_SHAPE,
  width: 40,
  height: 40,
  ports: PORTS,
  component: NodeEnd,
  data: {
    label: '结束'
  }
};
