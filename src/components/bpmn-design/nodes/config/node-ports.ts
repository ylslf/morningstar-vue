const ATTRS = {
  circle: {
    magnet: true,
    stroke: '#8f8f8f',
    r: 4
  }
};

export const PORTS = {
  groups: {
    right: {
      position: 'right',
      attrs: ATTRS
    },
    left: {
      position: 'left',
      attrs: ATTRS
    },
    top: {
      position: 'top',
      attrs: ATTRS
    },
    bottom: {
      position: 'bottom',
      attrs: ATTRS
    }
  }
};