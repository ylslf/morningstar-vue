import { PORTS } from '@/components/bpmn-design/nodes/config/node-ports';
import NodeExclusiveGateway from '@/components/bpmn-design/nodes/node-exclusive-gateway/node-exclusive-gateway.vue';

export const NODE_EXCLUSIVE_GATEWAY_SHAPE = 'node_exclusive_gateway';

export const NODE_EXCLUSIVE_GATEWAY = {
  shape: NODE_EXCLUSIVE_GATEWAY_SHAPE,
  label: '排他网关',
  ports: {
    items: [
      {
        id: 'port_left',
        group: 'left'
      },
      {
        id: 'port_right',
        group: 'right'
      },
      {
        id: 'port_top',
        group: 'top'
      },
      {
        id: 'port_bottom',
        group: 'bottom'
      }
    ]
  }
};

export const NODE_EXCLUSIVE_GATEWAY_DEFINE = {
  shape: NODE_EXCLUSIVE_GATEWAY_SHAPE,
  width: 40,
  height: 40,
  ports: PORTS,
  component: NodeExclusiveGateway,
  data: {
    label: '排他网关'
  }
};