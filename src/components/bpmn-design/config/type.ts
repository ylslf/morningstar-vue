export interface CONDITION_GROUP {
  relationship: boolean;
  groups: Array<GROUP>;
}

export interface GROUP {
  key: string,
  name: string,
  relationship: boolean;
  conditions: Array<CONDITION>
}

export interface CONDITION {
  key: string,
  name: string,
  type: string,
  value: string
}