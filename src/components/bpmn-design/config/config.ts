export const ASSIGN_STRATEGY = [
  { value: 'START_USER', label: '流程发起人' },

  { value: 'SELECT_USER', label: '指定用户' },
  { value: 'SELECT_ROLE', label: '指定角色' },
  { value: 'SELECT_DEPT', label: '指定部门' },
  { value: 'SELECT_POST', label: '指定职位' },

  { value: 'SELECT_DEPT_LEADER', label: '部门负责人' }
];

export const OPERATE_LIST = [
  { value: 'AGREE', label: '同意', desc: '同意审批' },
  { value: 'REFUSE', label: '拒绝', desc: '拒绝审批' },
  { value: 'TURN_TO', label: '转办', desc: '将该任务给其他人' }
];

export const CONDITION_COLUMNS = [
  {
    title: '名称',
    dataIndex: 'name',
    slotName: 'name',
    width: 150
  }, {
    title: '类型',
    dataIndex: 'type',
    slotName: 'type',
    width: 160
  }, {
    title: 'SPEL表达式/类',
    dataIndex: 'value',
    slotName: 'value',
    width: 250
  }, {
    title: '操作',
    dataIndex: 'operate',
    slotName: 'operate',
    titleSlotName: 'titleOperate',
    align: 'center',
    width: 30
  }
];