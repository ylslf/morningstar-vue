export const CONNECTING_CONFIG = {
  snap: true,
  allowBlank: false,
  allowLoop: true,
  allowMulti: false
};

export const BACKGROUND_CONFIG = {
  color: '#F2F7FA'
};

export const GRID_CONFIG = {
  visible: true,
  type: 'doubleMesh',
  args: [
    {
      color: '#eee',
      thickness: 1
    },
    {
      color: '#ddd',
      thickness: 1,
      factor: 4
    }
  ]
};

// 预览流程图配置
export const GRID_CONFIG_PREVIEW = {
  visible: false,
  type: 'doubleMesh',
  args: [
    {
      color: '#eee',
      thickness: 1
    },
    {
      color: '#ddd',
      thickness: 1,
      factor: 4
    }
  ]
};

export const STENCIL_LAYOUT_OPTIONS = {
  resizeToFit: false
};

export const STENCIL_GROUPS_CONFIG = [
  {
    name: 'event',
    title: '事件',
    collapsable: true
  },
  {
    name: 'task',
    title: '任务',
    collapsable: true
  },
  {
    name: 'gateway',
    title: '网关',
    collapsable: true
  }
];

export const TOOLS_NODE = [
  {
    name: 'boundary',
    args: {
      attrs: {
        fill: '#7c68fc',
        stroke: '#333',
        'stroke-width': 1,
        'fill-opacity': 0.2
      }
    }
  },
  {
    name: 'button-remove',
    args: {
      x: 0,
      y: 0,
      offset: { x: 5, y: 5 }
    }
  }
];

export const TOOLS_EDGE = [
  {
    name: 'vertices',
    args: {
      stopPropagation: false
    }
  },
  'segments',
  'button-remove',
  'boundary'
];