import { App } from 'vue';
import { use } from 'echarts/core';
import { CanvasRenderer } from 'echarts/renderers';
import { BarChart, LineChart, PieChart, RadarChart } from 'echarts/charts';
import {
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent
} from 'echarts/components';

import GridContainer from '@/components/form-design/form-container/container-item/grid-container.vue';
import GridSetting from '@/components/form-design/item-setting/container-setting/grid-setting.vue';
import GridColSetting from '@/components/form-design/item-setting/container-setting/grid-col-setting.vue';
import TabsContainer from '@/components/form-design/form-container/container-item/tabs-container.vue';
import TabsContainerSetting from '@/components/form-design/item-setting/container-setting/tabs-container-setting.vue';
// 输入框
import FormItemInput from '@/components/form-design/form-container/form-item/form-item-input.vue';
import ItemSettingInput from '@/components/form-design/item-setting/form-item-setting/item-setting-input.vue';
// 数组输入框
import FormItemInputNumber from '@/components/form-design/form-container/form-item/form-item-input-number.vue';
import ItemSettingInputNumber
  from '@/components/form-design/item-setting/form-item-setting/item-setting-input-number.vue';
// 文本域
import FormItemTextarea from '@/components/form-design/form-container/form-item/form-item-textarea.vue';
import ItemSettingTextarea from '@/components/form-design/item-setting/form-item-setting/item-setting-textarea.vue';
// 下拉选
import FormItemSelect from '@/components/form-design/form-container/form-item/form-item-select.vue';
import ItemSettingSelect from '@/components/form-design/item-setting/form-item-setting/item-setting-select.vue';
// 复选框
import FormItemCheckBox from '@/components/form-design/form-container/form-item/form-item-check-box.vue';
import ItemSettingCheckBox from '@/components/form-design/item-setting/form-item-setting/item-setting-check-box.vue';
// 单选框
import FormItemRadio from '@/components/form-design/form-container/form-item/form-item-radio.vue';
import ItemSettingRadio from '@/components/form-design/item-setting/form-item-setting/item-setting-radio.vue';
// 级联下拉选
import FormItemCascader from '@/components/form-design/form-container/form-item/form-item-cascader.vue';
import ItemSettingCascader from '@/components/form-design/item-setting/form-item-setting/item-setting-cascader.vue';
// 树选择
import FormItemTreeSelect from '@/components/form-design/form-container/form-item/form-item-tree-select.vue';
import ItemSettingTreeSelect
  from '@/components/form-design/item-setting/form-item-setting/item-setting-tree-select.vue';
// 颜色选择器
import FormItemColorPicker from '@/components/form-design/form-container/form-item/form-item-color-picker.vue';
import ItemSettingColorPicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-color-picker.vue';
// 评分
import FormItemRate from '@/components/form-design/form-container/form-item/form-item-rate.vue';
import ItemSettingRate from '@/components/form-design/item-setting/form-item-setting/item-setting-rate.vue';
// 滑动输入条
import FormItemSlider from '@/components/form-design/form-container/form-item/form-item-slider.vue';
import ItemSettingSlider from '@/components/form-design/item-setting/form-item-setting/item-setting-slider.vue';
// 开关
import FormItemSwitch from '@/components/form-design/form-container/form-item/form-item-switch.vue';
import ItemSettingSwitch from '@/components/form-design/item-setting/form-item-setting/item-setting-switch.vue';
// 日期选择器
import FormItemDatePicker from '@/components/form-design/form-container/form-item/form-item-date-picker.vue';
import ItemSettingDatePicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-date-picker.vue';
import FormItemMonthPicker from '@/components/form-design/form-container/form-item/form-item-month-picker.vue';
import ItemSettingMonthPicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-month-picker.vue';
import FormItemYearPicker from '@/components/form-design/form-container/form-item/form-item-year-picker.vue';
import ItemSettingYearPicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-year-picker.vue';
import FormItemQuarterPicker from '@/components/form-design/form-container/form-item/form-item-quarter-picker.vue';
import ItemSettingQuarterPicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-quarter-picker.vue';
// 时间选择器
import FormItemDateRangePicker from '@/components/form-design/form-container/form-item/form-item-date-range-picker.vue';
import ItemSettingDateRangePicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-date-range-picker.vue';
// 时间选择器
import FormItemTimePicker from '@/components/form-design/form-container/form-item/form-item-time-picker.vue';
import ItemSettingTimePicker
  from '@/components/form-design/item-setting/form-item-setting/item-setting-time-picker.vue';
// 上传图片
import FormItemImageUpload from '@/components/form-design/form-container/form-item/form-item-image-upload.vue';
import ItemSettingImageUpload
  from '@/components/form-design/item-setting/form-item-setting/item-setting-image-upload.vue';
// 上传文件
import FormItemFileUpload from '@/components/form-design/form-container/form-item/form-item-file-upload.vue';
import ItemSettingFileUpload
  from '@/components/form-design/item-setting/form-item-setting/item-setting-file-upload.vue';
// 用户选择
import FormItemUserSelect from '@/components/form-design/form-container/form-item/form-item-user-select.vue';
import ItemSettingUserSelect
  from '@/components/form-design/item-setting/form-item-setting/item-setting-user-select.vue';
// 角色选择
import FormItemRoleSelect from '@/components/form-design/form-container/form-item/form-item-role-select.vue';
import ItemSettingRoleSelect
  from '@/components/form-design/item-setting/form-item-setting/item-setting-role-select.vue';
// 部门选择
import FormItemDeptSelect from '@/components/form-design/form-container/form-item/form-item-dept-select.vue';
import ItemSettingDeptSelect
  from '@/components/form-design/item-setting/form-item-setting/item-setting-dept-select.vue';

import GridContainerRender from '@/components/form-render/container-item/grid-container.vue';
import TabsContainerRender from '@/components/form-render/container-item/tabs-container.vue';

import Chart from './chart/index.vue';
import Breadcrumb from './breadcrumb/index.vue';

import ColumnOperator from './table/column-operator/index.vue';
import HeaderOperator from './table/header-operator/index.vue';
import HeaderQuery from './table/heaer-query/index.vue';

// Manually introduce ECharts modules to reduce packing size

use([
  CanvasRenderer,
  BarChart,
  LineChart,
  PieChart,
  RadarChart,
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent
]);

export default {
  install(Vue: App) {
    Vue.component('Chart', Chart);
    Vue.component('Breadcrumb', Breadcrumb);

    Vue.component('ColumnOperator', ColumnOperator);
    Vue.component('HeaderOperator', HeaderOperator);
    Vue.component('HeaderQuery', HeaderQuery);

    // 容器组件
    Vue.component('GridContainer', GridContainer);
    Vue.component('GridSetting', GridSetting);
    Vue.component('GridColSetting', GridColSetting);
    Vue.component('TabsContainer', TabsContainer);
    Vue.component('TabsContainerSetting', TabsContainerSetting);

    // 基本组件
    Vue.component('FormItemInput', FormItemInput);
    Vue.component('ItemSettingInput', ItemSettingInput);
    Vue.component('FormItemInputNumber', FormItemInputNumber);
    Vue.component('ItemSettingInputNumber', ItemSettingInputNumber);
    Vue.component('FormItemSelect', FormItemSelect);
    Vue.component('ItemSettingSelect', ItemSettingSelect);
    Vue.component('FormItemTextarea', FormItemTextarea);
    Vue.component('ItemSettingTextarea', ItemSettingTextarea);
    Vue.component('FormItemDatePicker', FormItemDatePicker);
    Vue.component('ItemSettingDatePicker', ItemSettingDatePicker);
    Vue.component('FormItemMonthPicker', FormItemMonthPicker);
    Vue.component('ItemSettingMonthPicker', ItemSettingMonthPicker);
    Vue.component('FormItemYearPicker', FormItemYearPicker);
    Vue.component('ItemSettingYearPicker', ItemSettingYearPicker);
    Vue.component('FormItemQuarterPicker', FormItemQuarterPicker);
    Vue.component('ItemSettingQuarterPicker', ItemSettingQuarterPicker);
    Vue.component('FormItemDateRangePicker', FormItemDateRangePicker);
    Vue.component('ItemSettingDateRangePicker', ItemSettingDateRangePicker);
    Vue.component('FormItemTimePicker', FormItemTimePicker);
    Vue.component('ItemSettingTimePicker', ItemSettingTimePicker);
    Vue.component('FormItemCheckBox', FormItemCheckBox);
    Vue.component('ItemSettingCheckBox', ItemSettingCheckBox);
    Vue.component('FormItemRadio', FormItemRadio);
    Vue.component('ItemSettingRadio', ItemSettingRadio);
    Vue.component('FormItemCascader', FormItemCascader);
    Vue.component('ItemSettingCascader', ItemSettingCascader);
    Vue.component('FormItemTreeSelect', FormItemTreeSelect);
    Vue.component('ItemSettingTreeSelect', ItemSettingTreeSelect);
    Vue.component('FormItemColorPicker', FormItemColorPicker);
    Vue.component('ItemSettingColorPicker', ItemSettingColorPicker);
    Vue.component('FormItemRate', FormItemRate);
    Vue.component('ItemSettingRate', ItemSettingRate);
    Vue.component('FormItemSlider', FormItemSlider);
    Vue.component('ItemSettingSlider', ItemSettingSlider);
    Vue.component('FormItemSwitch', FormItemSwitch);
    Vue.component('ItemSettingSwitch', ItemSettingSwitch);
    Vue.component('FormItemImageUpload', FormItemImageUpload);
    Vue.component('ItemSettingImageUpload', ItemSettingImageUpload);
    Vue.component('FormItemFileUpload', FormItemFileUpload);
    Vue.component('ItemSettingFileUpload', ItemSettingFileUpload);
    Vue.component('FormItemUserSelect', FormItemUserSelect);
    Vue.component('ItemSettingUserSelect', ItemSettingUserSelect);
    Vue.component('FormItemRoleSelect', FormItemRoleSelect);
    Vue.component('ItemSettingRoleSelect', ItemSettingRoleSelect);
    Vue.component('FormItemDeptSelect', FormItemDeptSelect);
    Vue.component('ItemSettingDeptSelect', ItemSettingDeptSelect);

    // 表单渲染容器
    Vue.component('GridContainerRender', GridContainerRender);
    Vue.component('TabsContainerRender', TabsContainerRender);
  }
};
