import shortId from 'shortid';

/**
 * 获取表单动态组件Id
 * @param type 组件类型
 */
export const getComponentId: (type: string) => string = function(type) {
  return `${type}_${shortId.generate()}`;
};