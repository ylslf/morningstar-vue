import { InjectionKey, Ref } from 'vue';
import {
  FormEventInfo,
  FormActionInfo,
  FormEventBusDefinition
} from '@/components/form-design/event/type';
import { ComponentConfig } from '@/components/form-design/item-select/config/type';
// 组件动作
export const PROVIDE_KEY_ACTION_MAP: InjectionKey<Map<string, FormActionInfo>> = Symbol('ACTION_MAP');

// 组件事件
export const PROVIDE_KEY_EVENT_MAP: InjectionKey<Map<string, FormEventInfo>> = Symbol('EVENT_MAP');

// 事件总线
export const PROVIDE_KEY_EVENT_BUS: InjectionKey<FormEventBusDefinition> = Symbol('EVENT_BUS');


// 当前选中组件
export const PROVIDE_KEY_SELECTED: InjectionKey<Ref<ComponentConfig>> = Symbol('SELECTED');

// 表单组件配置 TODO 类型
export const PROVIDE_KEY_FORM_CONFIG = Symbol('FROM_CONFIG');

// 表单实例 TODO 类型
export const PROVIDE_KEY_FROM_INSTANCE = Symbol('FROM_INSTANCE');

// 表单类型
export const PROVIDE_KEY_FORM_TYPE: InjectionKey<string> = Symbol('FROM_TYPE');

// 表单权限 TODO 类型
export const PROVIDE_KEY_FORM_PERMISSION: InjectionKey<any> = Symbol('FORM_PERMISSION');