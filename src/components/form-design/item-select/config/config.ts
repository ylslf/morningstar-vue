import { cloneDeep } from 'lodash';
import { ComponentConfig } from '@/components/form-design/item-select/config/type';

/**
 * 容器组件
 */
export const BASE_GRID_COL_CONFIG: ComponentConfig = {
  name: '栅格列',
  group: 'container',
  type: 'grid-col',
  component: 'grid-container',
  settingComponent: 'grid-col-setting',
  selectIcon: undefined,
  componentSetting: {
    span: 12,
    offset: 0,
    order: undefined
  },
  subComponents: []
};

export const GRID_CONFIG: ComponentConfig = {
  name: '栅格',
  group: 'container',
  type: 'grid',
  component: 'grid-container',
  settingComponent: 'grid-setting',
  selectIcon: 'icon-apps',
  componentSetting: {
    gutter: 0,
    justify: 'start',
    align: 'start',
    wrap: true
  },
  subComponents: [cloneDeep(BASE_GRID_COL_CONFIG), cloneDeep(BASE_GRID_COL_CONFIG)]
};

export const DEFAULT_TAB_CONFIG: ComponentConfig = {
  name: '标签页',
  group: 'container',
  type: 'tab',
  component: '',
  settingComponent: '',
  selectIcon: '',
  componentSetting: {
    name: '标签页'
  },
  subComponents: []
};

export const TABS_CONFIG: ComponentConfig = {
  name: '标签页',
  group: 'container',
  type: 'tabs',
  component: 'tabs-container',
  settingComponent: 'tabs-container-setting',
  selectIcon: 'icon-folder',
  componentSetting: {
    size: 'medium',
    type: 'line',
    position: 'top',
    trigger: 'click',
    justify: false,
    animation: false,
    hideContent: false,
    headerPadding: true,
    defaultActiveKey: ''
  },
  subComponents: [cloneDeep(DEFAULT_TAB_CONFIG)]
};

/**
 * 基础组件
 */
export const INPUT_CONFIG: ComponentConfig = {
  name: '输入框',
  group: 'basic',
  type: 'input',
  component: 'form-item-input',
  settingComponent: 'item-setting-input',
  selectIcon: 'icon-pen-fill',
  formItemSetting: {
    tooltip: '',
    label: '输入框',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    maxLength: '',
    showWordLimit: true,
    onChangeFunction: ''
  }
};

export const INPUT_NUMBER_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'input-number',
  component: 'form-item-input-number',
  settingComponent: 'item-setting-input-number',
  name: '数字输入框',
  selectIcon: 'icon-formula',
  formItemSetting: {
    tooltip: '',
    label: '数字输入框',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: undefined,
    placeholder: '',
    allowClear: true,
    mode: 'embed',
    precision: undefined,
    step: undefined,
    max: undefined,
    min: undefined,
    hideButton: false,
    formatter: '',
    parser: '',
    onChangeFunction: ''
  }
};

export const SELECT_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'select',
  component: 'form-item-select',
  settingComponent: 'item-setting-select',
  name: '选择器',
  selectIcon: 'icon-down',
  formItemSetting: {
    tooltip: '',
    label: '选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: undefined,
    placeholder: '',
    allowClear: true,
    multiple: false,
    allowSearch: true,
    maxTagCount: 0,
    bordered: true,
    limit: 5,
    options: [],
    onChangeFunction: ''
  }
};

export const TEXTAREA_CONFIG: ComponentConfig = {
  id: undefined,
  name: '文本域',
  group: 'basic',
  type: 'textarea',
  component: 'form-item-textarea',
  settingComponent: 'item-setting-textarea',
  selectIcon: 'icon-paste',
  formItemSetting: {
    tooltip: '',
    label: '文本域',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    maxLength: '',
    showWordLimit: true,
    autoSize: true,
    onChangeFunction: ''
  }
};

export const CHECK_BOX_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'check-box',
  component: 'form-item-check-box',
  settingComponent: 'item-setting-check-box',
  name: '复选框',
  selectIcon: 'icon-check-square',
  formItemSetting: {
    tooltip: '',
    label: '复选框',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    direction: 'horizontal',
    max: undefined,
    checkAll: false,
    options: [],
    onChangeFunction: ''
  }
};

export const RADIO_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'radio',
  component: 'form-item-radio',
  settingComponent: 'item-setting-radio',
  name: '单选框',
  selectIcon: 'icon-record',
  formItemSetting: {
    tooltip: '',
    label: '单选框',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    direction: 'horizontal',
    type: 'radio',
    options: [],
    onChangeFunction: ''
  }
};

export const CASCADER_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'cascader',
  component: 'form-item-cascader',
  settingComponent: 'item-setting-cascader',
  name: '级联选择',
  selectIcon: 'icon-double-down',
  formItemSetting: {
    tooltip: '',
    label: '级联选择',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    placeholder: '',
    allowClear: true,
    options: '',
    multiple: false,
    allowSearch: false,
    pathMode: false,
    maxTagCount: 5,
    checkStrictly: false,
    onChangeFunction: ''
  }
};

export const TREE_SELECT_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'tree-select',
  component: 'form-item-tree-select',
  settingComponent: 'item-setting-tree-select',
  name: '树选择',
  selectIcon: 'icon-mind-mapping',
  formItemSetting: {
    tooltip: '',
    label: '树选择',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    placeholder: '',
    allowClear: true,
    options: '',
    multiple: false,
    allowSearch: false,
    maxTagCount: 5,
    border: true,
    treeCheckable: false,
    treeCheckStrictly: false,
    treeCheckedStrategy: 'all',
    onChangeFunction: ''
  }
};

export const COLOR_PICKER_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'color-picker',
  component: 'form-item-color-picker',
  settingComponent: 'item-setting-color-picker',
  name: '颜色选择器',
  selectIcon: 'icon-bg-colors',
  formItemSetting: {
    tooltip: '',
    label: '颜色选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    format: 'hex',
    showText: false,
    showHistory: false,
    disabledAlpha: false,
    onChangeFunction: ''
  }
};

export const RATE_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'rate',
  component: 'form-item-rate',
  settingComponent: 'item-setting-rate',
  name: '评分',
  selectIcon: 'icon-star',
  formItemSetting: {
    tooltip: '',
    label: '评分',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    count: 5,
    allowHalf: false,
    allowClear: true,
    grading: false,
    color: '#425FFF',
    onChangeFunction: ''
  }
};

export const SLIDER_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'slider',
  component: 'form-item-slider',
  settingComponent: 'item-setting-slider',
  name: '滑动输入条',
  selectIcon: 'icon-minus',
  formItemSetting: {
    tooltip: '',
    label: '滑动输入条',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: 0,
    direction: 'horizontal',
    step: 1,
    min: 0,
    max: 100,
    showTicks: false,
    showInput: false,
    range: false,
    showTooltip: false,
    marks: '',
    onChangeFunction: ''
  }
};

export const SWITCH_CONFIG: ComponentConfig = {
  group: 'basic',
  type: 'switch',
  component: 'form-item-switch',
  settingComponent: 'item-setting-switch',
  name: '开关',
  selectIcon: 'icon-poweroff',
  formItemSetting: {
    tooltip: '',
    label: '开关',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    type: 'circle',
    checkedValue: true,
    uncheckedValue: false,
    checkedColor: '',
    uncheckedColor: '',
    checkedText: '',
    uncheckedText: '',
    onChangeFunction: ''
  }
};

export const DATE_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '日期选择器',
  group: 'basic',
  type: 'date-picker',
  component: 'form-item-date-picker',
  settingComponent: 'item-setting-date-picker',
  selectIcon: 'icon-calendar-clock',
  formItemSetting: {
    tooltip: '',
    label: '日期选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    position: 'bottom',
    format: undefined,
    showTime: true,
    disabledDate: '',
    disabledTime: '',
    onChangeFunction: ''
  }
};

export const MONTH_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '月份选择器',
  group: 'basic',
  type: 'month-picker',
  component: 'form-item-month-picker',
  settingComponent: 'item-setting-month-picker',
  selectIcon: 'icon-calendar',
  formItemSetting: {
    tooltip: '',
    label: '月份选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    position: 'bottom',
    format: undefined,
    onChangeFunction: ''
  }
};

export const YEAR_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '年份选择器',
  group: 'basic',
  type: 'year-picker',
  component: 'form-item-year-picker',
  settingComponent: 'item-setting-year-picker',
  selectIcon: 'icon-calendar',
  formItemSetting: {
    tooltip: '',
    label: '年份选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    position: 'bottom',
    format: undefined,
    onChangeFunction: ''
  }
};

export const QUARTER_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '季度选择器',
  group: 'basic',
  type: 'quarter-picker',
  component: 'form-item-quarter-picker',
  settingComponent: 'item-setting-quarter-picker',
  selectIcon: 'icon-calendar',
  formItemSetting: {
    tooltip: '',
    label: '季度选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    position: 'bottom',
    format: undefined,
    onChangeFunction: ''
  }
};

export const DATE_RANGE_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '日期范围',
  group: 'basic',
  type: 'date-range-picker',
  component: 'form-item-date-range-picker',
  settingComponent: 'item-setting-date-range-picker',
  selectIcon: 'icon-calendar-clock',
  formItemSetting: {
    tooltip: '',
    label: '日期范围',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    mode: 'date',
    position: 'bottom',
    format: undefined,
    showTime: true,
    separator: undefined,
    abbreviation: false,
    disabledDate: '',
    disabledTime: '',
    onChangeFunction: ''
  }
};

export const TIME_PICKER_CONFIG: ComponentConfig = {
  id: '',
  name: '时间选择器',
  group: 'basic',
  type: 'time-picker',
  component: 'form-item-time-picker',
  settingComponent: 'item-setting-time-picker',
  selectIcon: 'icon-schedule',
  formItemSetting: {
    tooltip: '',
    label: '时间选择器',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: '',
    placeholder: '',
    allowClear: true,
    type: 'time',
    format: undefined,
    position: 'bottom',
    use12Hours: false,
    step: {
      hour: 1,
      minute: 1,
      second: 1
    },
    disabledHours: '',
    disabledMinutes: '',
    disabledSeconds: '',
    hideDisabledOptions: false,
    disableConfirm: false,
    onChangeFunction: ''
  }
};

/**
 * 高级组件
 */
export const IMAGE_UPLOAD_CONFIG: ComponentConfig = {
  group: 'advanced',
  type: 'image-upload',
  component: 'form-item-image-upload',
  settingComponent: 'item-setting-image-upload',
  name: '上传图片',
  selectIcon: 'icon-image',
  formItemSetting: {
    tooltip: '',
    label: '上传图片',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    multiple: false,
    directory: false,
    draggable: false,
    limit: 0,
    autoUpload: true,
    showFileList: true,
    showRemoveButton: true,
    showRetryButton: true,
    showCancelButton: true,
    showUploadButton: true,
    showPreviewButton: true,
    download: false,
    showLink: true,
    imageLoading: '',
    listType: 'picture-card',
    imagePreview: true,
    onChangeFunction: ''
  }
};

export const FILE_UPLOAD_CONFIG: ComponentConfig = {
  group: 'advanced',
  type: 'file-upload',
  component: 'form-item-file-upload',
  settingComponent: 'item-setting-file-upload',
  name: '上传文件',
  selectIcon: 'icon-file-image',
  formItemSetting: {
    tooltip: '',
    label: '上传文件',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    multiple: false,
    directory: false,
    draggable: false,
    limit: 0,
    autoUpload: true,
    showFileList: true,
    showRemoveButton: true,
    showRetryButton: true,
    showCancelButton: true,
    showUploadButton: true,
    showPreviewButton: true,
    download: true,
    showLink: true,
    imageLoading: '',
    listType: 'text',
    imagePreview: false,
    onChangeFunction: ''
  }
};

/**
 * 业务组件
 */
export const USER_SELECT_CONFIG: ComponentConfig = {
  group: 'system',
  type: 'user-select',
  component: 'form-item-user-select',
  settingComponent: 'item-setting-user-select',
  name: '用户选择',
  selectIcon: 'icon-user',
  formItemSetting: {
    tooltip: '',
    label: '用户选择',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: undefined,
    placeholder: '',
    allowClear: true,
    multiple: false,
    allowSearch: true,
    maxTagCount: 0,
    bordered: true,
    limit: 5,
    onChangeFunction: ''
  }


};

export const ROLE_SELECT_CONFIG: ComponentConfig = {
  group: 'system',
  type: 'role-select',
  component: 'form-item-role-select',
  settingComponent: 'item-setting-role-select',
  name: '角色选择',
  selectIcon: 'icon-user-group',
  formItemSetting: {
    tooltip: '',
    label: '角色选择',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: undefined,
    placeholder: '',
    allowClear: true,
    multiple: false,
    allowSearch: true,
    maxTagCount: 0,
    bordered: true,
    limit: 5,
    onChangeFunction: ''
  }
};

export const DEPT_SELECT_CONFIG: ComponentConfig = {
  group: 'system',
  type: 'dept-select',
  component: 'form-item-dept-select',
  settingComponent: 'item-setting-dept-select',
  name: '部门选择',
  selectIcon: 'icon-bar-chart',
  formItemSetting: {
    tooltip: '',
    label: '部门选择',
    labelColFlex: '',
    showColon: false,
    required: false,
    ruleType: '',
    ruleMessage: '',
    feedback: false,
    ruleMatch: '',
    validateTrigger: 'change',
    asteriskPosition: 'start',
    hideLabel: false,
    hideAsterisk: false
  },
  componentSetting: {
    defaultValue: [],
    placeholder: '',
    allowClear: true,
    multiple: false,
    allowSearch: false,
    maxTagCount: 5,
    border: true,
    treeCheckable: false,
    treeCheckStrictly: false,
    treeCheckedStrategy: 'all',
    onChangeFunction: ''
  }
};

/**
 * 组件列表配置
 */
export const CONTAINER_CONFIG: ComponentConfig[] = [
  GRID_CONFIG,
  TABS_CONFIG
];

export const BASIC_CONFIG: ComponentConfig[] = [
  INPUT_CONFIG,
  INPUT_NUMBER_CONFIG,
  TEXTAREA_CONFIG,
  SELECT_CONFIG,
  CHECK_BOX_CONFIG,
  RADIO_CONFIG,
  CASCADER_CONFIG,
  TREE_SELECT_CONFIG,
  COLOR_PICKER_CONFIG,
  RATE_CONFIG,
  SLIDER_CONFIG,
  SWITCH_CONFIG,
  DATE_PICKER_CONFIG,
  MONTH_PICKER_CONFIG,
  YEAR_PICKER_CONFIG,
  QUARTER_PICKER_CONFIG,
  DATE_RANGE_PICKER_CONFIG,
  TIME_PICKER_CONFIG
];

export const ADVANCED_CONFIG: ComponentConfig[] = [
  IMAGE_UPLOAD_CONFIG,
  FILE_UPLOAD_CONFIG
];

export const SYSTEM_CONFIG: ComponentConfig[] = [
  USER_SELECT_CONFIG,
  ROLE_SELECT_CONFIG,
  DEPT_SELECT_CONFIG
];