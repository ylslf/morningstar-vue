/**
 * container 容器
 * basic 基本
 * advanced 高级
 * system 系统组件
 * 表单分组
 */
export type ComponentGroup = 'container' | 'basic' | 'advanced' | 'system';


/**
 * 表单 formItem 配置  参考 arco design FormItem 配置
 */
export interface FormItemSetting {
  /**
   * 标签提示
   */
  tooltip?: string,
  /**
   * 标签文本
   */
  label?: string,
  /**
   * 标签宽度
   */
  labelColFlex?: any,
  /**
   * 星号位置
   */
  showColon?: boolean,
  /**
   * 是否必填
   */
  required?: boolean,
  /**
   * 校验值类型
   */
  ruleType?: string,
  /**
   * 校验失败提示
   */
  ruleMessage?: string,
  /**
   * 反馈图标
   */
  feedback?: boolean,
  /**
   * 正则表达式
   */
  ruleMatch?: string,
  /**
   * 触发校验事件
   */
  validateTrigger?: string,
  /**
   * 星号位置
   */
  asteriskPosition?: string,
  /**
   * 隐藏标签
   */
  hideLabel?: boolean,
  /**
   * 隐藏星号
   */
  hideAsterisk?: boolean
}

/**
 * 组件配置
 */
export interface ComponentConfig {
  /**
   * 表单组件全局Id
   */
  id?: string,
  /**
   * 组件名称
   */
  name: string,
  /**
   * 分组  container, basic, advanced, system
   */
  group: ComponentGroup,
  /**
   * 类型  grid  ??? 好像没用
   */
  type: string,
  /**
   * 对应组件名称
   */
  component: string,
  /**
   * 对应配置组件
   */
  settingComponent: string,
  /**
   * 图标
   */
  selectIcon?: string,
  /**
   * 表单 formItem 配置
   */
  formItemSetting?: FormItemSetting,
  /**
   * 组件个性配置
   */
  componentSetting?: any,
  /**
   * 子组件列表
   */
  subComponents?: ComponentConfig[],
}

/**
 * 表单配置校验结果
 */
export interface ItemValidateResult {
  /**
   * 表单校验结果 标志位
   */
  status: boolean,
  /**
   * 表单校验结果 消息
   */
  message?: string
}

/**
 * 表单权限
 * H 隐藏
 * R 只读
 * D 禁用
 * E 编辑
 */
export type FormPermission = 'H' | 'R' | 'D' | 'E';