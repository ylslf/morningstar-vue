import { Position } from '@vue-flow/core';

/**
 * 表单注册动作定义
 */
export interface FormActionDefinition {
  /**
   * 表单动作动作名称
   */
  name: string;
  /**
   * 表单动作KEY
   */
  key: string;
  /**
   * 执行动作函数 ？？？ 是否可以通过函数获取入参
   */
  action: Function;
  /**
   * 动作函数入参 类型描述
   */
  args: Array<any>;

  /**
   * 动作返回结果 类型描述
   */
  result: Array<any>;
}

export interface FormActionInfo {
  /**
   * 表单标签名称
   */
  formLabel?: string;

  actionList?: Array<FormActionDefinition>;
}

/**
 * 表单注册事件定义
 */
export interface FormEventDefinition {
  /**
   * 事件名称
   */
  name: string,
  /**
   * 事件KEY 单个组件内唯一
   */
  key: string,
  /**
   * 事件抛出的数据 类型描述
   */
  args?: Array<any>
}


export interface FormEventInfo {
  /**
   * 表单标签名称
   */
  formLabel?: string;

  eventList?: Array<FormEventDefinition>;
}


/**
 * 事件总线定义
 */
export interface FormEventBusDefinition {
  eventConfigs: any,
  submitEvent: Function;
}

/**
 * 事件总线配置
 * 采用链表 数据结构 也可以是树
 *
 * EventBus 通过事件抛出的 sourceFormId sourceEventKey 查找事件配置
 *
 * [
 *  {
 *    sourceId: undefined,
 *    formId: 1-1
 *    eventKey: 1-1
 *    targetId: 2,
 *    children: [
 *      {
 *        sourceId: 2
 *        formId: 1-2
 *        eventKey: 1-2
 *        targetId: 3,
 *        children:[]
 *      },
 *    ]
 *  }
 * ]
 *
 */
export interface FormEventBusConfig {
  /**
   * 配置类型
   * 表单事件 formEvent
   * 聚合值事件 polymerizedEvent ???
   * 用于多个表单组件通过处理后的结果
   * 类似于：计算两个input的和结果赋予另一个input
   */
  type: string,
  /**
   * 事件链表头
   * sourceId 为空则是头部
   */
  sourceId?: string,
  /**
   * 事件链表尾
   * targetId 为空则为尾 或者末端节点
   */
  targetId?: string,
  /**
   * 事件目标表单Id
   * @see ComponentConfig
   */
  formId?: string,
  /**
   * 事件目标 key
   * @see ComponentConfig
   */
  eventKey?: string,
  /**
   * 下级事件
   */
  children: Array<FormEventBusConfig>
}

/**
 * 表单事件
 */
export interface FormEvent {
  /**
   * 事件源表单Id
   * @see ComponentConfig
   */
  sourceFormId: string,
  /**
   * 事件源 KEY
   * @see FormEventDefinition
   */
  sourceEventKey: string,
  /**
   * 事件抛出的参数
   */
  args: any,
}

export interface EventConfigItem {
  id: string,
  name: string,
  // 事件节点配置
  configs?: any,
}


export interface NodeData {
  toolbarVisible: boolean;
  toolbarPosition: Position;
  formId?: string;
  eventKey?: string;
}

export interface Props {
  id: string,
  data: NodeData;
  label: string;
}


export interface SelectOptions {
  value: string,
  label?: string
}