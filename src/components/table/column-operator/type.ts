export interface ColumnOperator {
  name: string;
  operator: string;
  icon: string;
  permission: Array<string>;
}