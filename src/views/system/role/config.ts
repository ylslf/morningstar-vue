import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.role.list',
  PERMISSION_DETAIL: 'system.role.detail',
  PERMISSION_UPDATE: 'system.role.update',
  PERMISSION_ADD: 'system.role.add',
  PERMISSION_DELETE: 'system.role.delete',
  PERMISSION_PERMISSION: 'system.role.permission'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '角色名称',
    name: 'roleName',
    placeholder: '请输入角色名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '角色编码',
    name: 'roleKey',
    placeholder: '请输入角色编码',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '权限',
    operator: 'permission',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_PERMISSION]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '角色名称',
    dataIndex: 'roleName'
  },
  {
    title: '角色编码',
    dataIndex: 'roleKey'
  },
  {
    title: '显示排序',
    dataIndex: 'roleSort',
    align: 'center'
  },
  {
    title: '数据权限',
    dataIndex: 'dataScope',
    slotName: 'dataScope',
    align: 'center'
  },
  {
    title: '状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center'
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    align: 'center'
  },
  {
    title: '备注',
    dataIndex: 'remark'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  roleName: [
    { required: true, message: '请输入角色名' }
  ],
  roleKey: [
    { required: true, message: '请输入角色编码' }
  ],
  roleSort: [
    { required: true, message: '请输入显示排序' }
  ],
  dataScope: [
    { required: true, message: '请选择数据权限' }
  ],
  status: [
    { required: true, message: '选择角色状态' }
  ]
};