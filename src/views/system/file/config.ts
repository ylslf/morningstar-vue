import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.file.list',
  PERMISSION_DETAIL: 'system.file.detail',
  PERMISSION_UPDATE: 'system.file.update',
  PERMISSION_ADD: 'system.file.add',
  PERMISSION_DELETE: 'system.file.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '文件名称',
    name: 'fileName',
    placeholder: '请输入文件名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '文件类型',
    name: 'fileType',
    placeholder: '请输入文件类型',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '文件名称',
    dataIndex: 'fileName',
    ellipsis: true,
    width: 300,
    tooltip: { position: 'left' }
  },
  {
    title: '文件类型',
    dataIndex: 'fileType',
    width: 100,
    align: 'center',
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '文件大小',
    align: 'center',
    dataIndex: 'fileSizeShow',
    width: 100
  },
  {
    title: '关联业务',
    align: 'center',
    dataIndex: 'fileBizNames',
    slotName: 'fileBizNames'
  },
  {
    title: '文件保存路径',
    dataIndex: 'filePath',
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '上传时间',
    dataIndex: 'createTime',
    align: 'center',
    width: 180
  },
  {
    title: '上传人',
    dataIndex: 'createByName',
    align: 'center',
    width: 120
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center',
    width: 120
  }
]);