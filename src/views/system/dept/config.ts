import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.dept.list',
  PERMISSION_DETAIL: 'system.dept.detail',
  PERMISSION_UPDATE: 'system.dept.update',
  PERMISSION_ADD: 'system.dept.add',
  PERMISSION_DELETE: 'system.dept.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '部门名称',
    name: 'deptName',
    placeholder: '请输入部门名称',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '新增',
    operator: 'add',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_ADD]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '部门名称',
    dataIndex: 'deptName'
  },
  {
    title: '负责人',
    dataIndex: 'leaderName'
  },
  {
    title: '负责人电话',
    dataIndex: 'phone',
    align: 'center'
  },
  {
    title: '负责人邮箱',
    dataIndex: 'email'
  },
  {
    title: '显示排序',
    dataIndex: 'orderNum',
    align: 'center'
  },
  {
    title: '状态',
    align: 'center',
    dataIndex: 'status',
    slotName: 'status'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);


export const rules = {
  parentId: [
    { required: true, message: '请选择上级部门' }
  ],
  deptName: [
    { required: true, message: '请填写部门名称' }
  ],
  leader: [
    { required: true, message: '请选择负责人' }
  ],
  orderNum: [
    { required: true, message: '请填写显示排序' }
  ],
  status: [
    { required: true, message: '请选择部门状态' }
  ]
};