import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_TYPE_LIST: 'system.dict.type.list',
  PERMISSION_TYPE_DETAIL: 'system.dict.type.detail',
  PERMISSION_TYPE_UPDATE: 'system.dict.type.update',
  PERMISSION_TYPE_ADD: 'system.dict.type.add',
  PERMISSION_TYPE_DELETE: 'system.dict.type.delete',

  PERMISSION_DATA_LIST: 'system.dict.data.list'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '字典类型名称',
    name: 'dictName',
    placeholder: '请输入字典类型名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '字典类型编码',
    name: 'dictType',
    placeholder: '请输入字典类型编码',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_TYPE_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '数据',
    operator: 'data',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DATA_LIST]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_TYPE_DETAIL, permission.PERMISSION_TYPE_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_TYPE_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '字典类型名称',
    dataIndex: 'dictName'
  },
  {
    title: '字典类型编码',
    dataIndex: 'dictType'
  },
  {
    title: '字典类型状态',
    align: 'center',
    dataIndex: 'status',
    slotName: 'status'
  },
  {
    title: '备注',
    dataIndex: 'remark'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  dictName: [
    { required: true, message: '请输入字典类型名称' }
  ],
  dictType: [
    { required: true, message: '请输入字典类型编码' }
  ]
};