import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_DATA_DETAIL: 'system.dict.data.detail',
  PERMISSION_DATA_UPDATE: 'system.dict.data.update',
  PERMISSION_DATA_ADD: 'system.dict.data.add',
  PERMISSION_DATA_DELETE: 'system.dict.data.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '字典标签',
    name: 'dictLabel',
    placeholder: '请输入字典标签',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '字典数据值',
    name: 'dictValue',
    placeholder: '请输入字典数据值',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_DATA_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DATA_DETAIL, permission.PERMISSION_DATA_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DATA_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '字典标签',
    align: 'center',
    dataIndex: 'dictLabel'
  },
  {
    title: '字典数据值',
    align: 'center',
    dataIndex: 'dictValue'
  },
  {
    title: '排序',
    align: 'center',
    dataIndex: 'dictSort'
  },
  {
    title: '默认选中',
    align: 'center',
    dataIndex: 'isDefault',
    slotName: 'isDefault'
  },
  {
    title: '状态',
    align: 'center',
    dataIndex: 'status',
    slotName: 'status'
  },
  {
    title: '样式',
    dataIndex: 'cssClass'
  },
  {
    title: '列表样式',
    dataIndex: 'listClass'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  dictLabel: [
    { required: true, message: '请输入字典标签' }
  ],
  dictValue: [
    { required: true, message: '请输入字典数据值' }
  ],
  dictSort: [
    { required: true, message: '请输入排序' }
  ]
};