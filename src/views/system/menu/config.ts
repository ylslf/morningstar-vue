import { ref, reactive, h, compile } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.menu.list',
  PERMISSION_DETAIL: 'system.menu.detail',
  PERMISSION_UPDATE: 'system.menu.update',
  PERMISSION_ADD: 'system.menu.add',
  PERMISSION_DELETE: 'system.menu.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '菜单名称',
    name: 'menuName',
    placeholder: '请输入菜单名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '路由名称',
    name: 'routeName',
    placeholder: '请输入路由名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '权限标识',
    name: 'perms',
    placeholder: '请输入权限标识',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '新增',
    operator: 'add',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_ADD]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '菜单名称',
    dataIndex: 'menuName'
  },
  {
    title: '菜单类型',
    align: 'center',
    dataIndex: 'menuType',
    slotName: 'menuType'
  },
  {
    title: '菜单图标',
    align: 'center',
    dataIndex: 'icon',
    render: ({ record }) => {
      return record.icon ? h(compile(`<${record?.icon}/>`)) : '';
    }
  },
  {
    title: '路由名称',
    dataIndex: 'routeName'
  },
  {
    title: '路由路径',
    dataIndex: 'routePath'
  },
  {
    title: '组件路径',
    dataIndex: 'routeComponent'
  },
  {
    title: '权限标识',
    dataIndex: 'perms'
  },
  {
    title: '是否刷新',
    align: 'center',
    dataIndex: 'isRefresh',
    slotName: 'isRefresh'

  },
  {
    title: '菜单状态',
    align: 'center',
    dataIndex: 'visible',
    slotName: 'visible'
  },
  {
    title: '显示排序',
    dataIndex: 'orderNum',
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  parentId: [
    { required: true, message: '请选择父级菜单' }
  ],
  menuName: [
    { required: true, message: '请输入菜单名称' }
  ],
  orderNum: [
    { required: true, message: '请输入显示排序' }
  ],
  menuType: [
    { required: true, message: '请选择菜单类型' }
  ],
  perms: [
    { required: true, message: '请输入权限标识' }
  ],
  icon: [
    { required: true, message: '请选择菜单图标' }
  ],
  routeName: [
    { required: true, message: '请输入路由名称' }
  ],
  routePath: [
    { required: true, message: '请输入路由地址' }
  ],
  routeComponent: [
    { required: true, message: '请输入组件地址' }
  ],
  visible: [
    { required: true, message: '请选择菜单状态' }
  ]
};