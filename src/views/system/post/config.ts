import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.post.list',
  PERMISSION_DETAIL: 'system.post.detail',
  PERMISSION_UPDATE: 'system.post.update',
  PERMISSION_ADD: 'system.post.add',
  PERMISSION_DELETE: 'system.post.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '职位名称',
    name: 'postName',
    placeholder: '请输入职位名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '职位编码',
    name: 'postCode',
    placeholder: '请输入职位编码',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '职位名称',
    dataIndex: 'postName'
  },
  {
    title: '职位编码',
    dataIndex: 'postCode'
  },
  {
    title: '显示排序',
    dataIndex: 'postSort',
    align: 'center'
  },
  {
    title: '状态',
    align: 'center',
    dataIndex: 'status',
    slotName: 'status'
  },
  {
    title: '备注',
    dataIndex: 'remark'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  postName: [
    { required: true, message: '请输入职位名称' }
  ],
  postCode: [
    { required: true, message: '请输入职位编码' }
  ],
  postSort: [
    { required: true, message: '请输入显示排序' }
  ],
  status: [
    { required: true, message: '请选择职位状态' }
  ]
};