import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.message.template.list',
  PERMISSION_DETAIL: 'system.message.template.detail',
  PERMISSION_UPDATE: 'system.message.template.update',
  PERMISSION_ADD: 'system.message.template.add',
  PERMISSION_DELETE: 'system.message.template.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '模板名称',
    name: 'templateName',
    placeholder: '请输入模板名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '模板编码',
    name: 'templateKey',
    placeholder: '请输入模板编码',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '模板名称',
    dataIndex: 'templateName'
  },
  {
    title: '模板编码',
    dataIndex: 'templateKey',
    ellipsis: true
  },
  {
    title: '消息模板',
    dataIndex: 'templateMessage',
    slotName: 'templateMessage',
    width: 180,
    ellipsis: true
  },
  {
    title: '状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center',
    width: 120
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    align: 'center',
    width: 180
  },
  {
    title: '备注',
    dataIndex: 'remark',
    ellipsis: true,
    width: 180
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center',
    width: 160
  }
]);

export const rules = {
  templateName: [
    { required: true, message: '请输入模板名称' }
  ],
  templateKey: [
    { required: true, message: '请输入模板编码' }
  ],
  templateMessage: [
    { required: true, message: '请输入消息模板' }
  ],
  sendType: [
    { required: true, message: '请选择发送类型' }
  ],
  status: [
    { required: true, message: '选择模板状态' }
  ]
};