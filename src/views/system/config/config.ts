import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.config.list',
  PERMISSION_DETAIL: 'system.config.detail',
  PERMISSION_UPDATE: 'system.config.update',
  PERMISSION_ADD: 'system.config.add',
  PERMISSION_DELETE: 'system.config.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '配置名称',
    name: 'configName',
    placeholder: '请输入配置名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '配置KEY',
    name: 'configKey',
    placeholder: '请输入配置KEY',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '配置名称',
    dataIndex: 'configName'
  },
  {
    title: '配置KEY',
    dataIndex: 'configKey'
  },
  {
    title: '配置值',
    dataIndex: 'configValue',
    align: 'center'
  },
  {
    title: '配置类型',
    dataIndex: 'configType',
    slotName: 'configType',
    align: 'center'
  },
  {
    title: '备注',
    dataIndex: 'remark',
    ellipsis: true,
    tooltip: true
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  configName: [
    { required: true, message: '请输入配置名称' }
  ],
  configKey: [
    { required: true, message: '请选择配置类型' }
  ],
  configValue: [
    { required: true, message: '请输入配置KEY' }
  ],
  configType: [
    { required: true, message: '请输入配置值' }
  ]
};