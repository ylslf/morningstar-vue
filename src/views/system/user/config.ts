import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';
import { ColumnOperator } from '@/components/table/column-operator/type';

export const permission = {
  PERMISSION_LIST: 'system.user.list',
  PERMISSION_DETAIL: 'system.user.detail',
  PERMISSION_UPDATE: 'system.user.update',
  PERMISSION_ADD: 'system.user.add',
  PERMISSION_DELETE: 'system.user.delete',
  PERMISSION_RESET_PASSWORD: 'system.user.reset.password'
};

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive<Array<ColumnOperator>>([
  {
    name: '重置密码',
    operator: 'reset',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_RESET_PASSWORD]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '登录名',
    dataIndex: 'loginName'
  },
  {
    title: '用户名',
    dataIndex: 'userName'
  },
  {
    title: '手机号',
    dataIndex: 'phone'
  },
  {
    title: '邮箱',
    dataIndex: 'email'
  },
  {
    title: '部门',
    dataIndex: 'deptName'
  },
  {
    title: '性别',
    dataIndex: 'sex',
    slotName: 'sex',
    align: 'center'
  },
  {
    title: '用户类型',
    dataIndex: 'userType',
    slotName: 'userType',
    align: 'center'
  },
  {
    title: '状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

/**
 * 表单校验规则
 */
export const rules = {
  loginName: [
    { required: true, message: '请输入登录名' },
    { minLength: 5, maxLength: 20, message: '登录名长度需在6到20个字符之间' },
    {
      match: new RegExp(/^(?=.*[a-zA-Z])[a-zA-Z0-9]+$/),
      message: '登录名必须包含至少一个字母，并且只能包含字母和数字'
    }
  ],
  userName: [
    { required: true, message: '请输入用户名' }
  ],
  email: [
    { required: true, message: '请输入正确的邮箱', type: 'email' }
  ],
  phone: [
    { required: true, message: '请输入手机号' },
    {
      match: new RegExp(/^1[3-9]\d{9}$/),
      message: '请输入正确的手机号'
    }
  ],
  deptId: [
    { required: true, message: '请选择部门' }
  ],
  postIds: [
    { required: true, message: '请选择职位' }
  ],
  roleIds: [
    { required: true, message: '请选择角色' }
  ],
  sex: [
    { required: true, message: '请选择用户性别' }
  ]
};