import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.message.list',
  PERMISSION_READ: 'system.message.read',
  PERMISSION_DELETE: 'system.message.delete',
  PERMISSION_READ_ALL: 'system.message.read.all'
};

export const queryConfig = reactive([
  {
    type: 'input',
    label: '消息标题',
    name: 'messageTitle',
    placeholder: '请输入消息标题',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '消息内容',
    name: 'messageContent',
    placeholder: '请输入消息内容',
    defaultValue: ''
  }
]);

export const headerOperators = reactive([
  {
    name: 'table.operator.read.all',
    operator: 'readAll',
    icon: 'icon-check',
    permission: permission.PERMISSION_READ_ALL
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '标记为已读',
    operator: 'read',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '消息标题',
    dataIndex: 'messageTitle',
    width: 260,
    ellipsis: true,
    tooltip: true
  },
  {
    title: '消息类型',
    dataIndex: 'messageType',
    align: 'center',
    slotName: 'messageType',
    width: 100
  },
  {
    title: '消息内容',
    dataIndex: 'messageContent',
    align: 'left',
    ellipsis: true,
    tooltip: true
  },
  {
    title: '状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center',
    width: 100
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    align: 'center',
    width: 180
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center',
    width: 150
  }
]);