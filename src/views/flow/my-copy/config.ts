import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_DETAIL: 'flow.instance.detail',
  PERMISSION_DELETE: 'flow.instance.delete',
  PERMISSION_REVOCATION: 'flow.instance.revocation',
  PERMISSION_START: 'flow.instance.start'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '流程名称',
    name: 'processName',
    placeholder: '配置名称',
    defaultValue: ''
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '流程编号',
    dataIndex: 'bizNumber'
  },
  {
    title: '流程名称',
    dataIndex: 'processName'
  },
  {
    title: '发起人',
    dataIndex: 'startUserName',
    align: 'center'
  },
  {
    title: '流程状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center'
  },
  {
    title: '发起时间',
    dataIndex: 'createTime',
    align: 'center'
  },
  {
    title: '结束时间',
    dataIndex: 'endTime',
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL]
  }
]);