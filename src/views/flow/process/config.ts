import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'flow.process.list',
  PERMISSION_DETAIL: 'flow.process.detail',
  PERMISSION_UPDATE: 'flow.process.update',
  PERMISSION_ADD: 'flow.process.add',
  PERMISSION_DELETE: 'flow.process.delete',
  PERMISSION_DEPLOY: 'flow.process.deploy',
  PERMISSION_UPDATE_STATUS: 'flow.process.update.status'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '流程名称',
    name: 'processName',
    placeholder: '请输入流程名称',
    defaultValue: ''
  },
  {
    type: 'input',
    label: '流程key',
    name: 'processKey',
    placeholder: '请输入流程KEY',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '设计流程',
    operator: 'design',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DEPLOY]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '流程名称',
    dataIndex: 'processName'
  },
  {
    title: '流程KEY',
    dataIndex: 'processKey'
  },
  // {
  //   title: '流程版本',
  //   dataIndex: 'processVersion',
  //   align: 'center'
  // },
  {
    title: '所属流程组',
    dataIndex: 'processGroupName'
  },
  {
    title: '绑定表单',
    dataIndex: 'formName'
  },
  {
    title: '流程图',
    dataIndex: 'deploy',
    slotName: 'deploy',
    align: 'center'
  },
  {
    title: '流程状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);

export const rules = {
  processName: [
    { required: true, message: '请输入流程名称' }
  ],
  processKey: [
    { required: true, message: '请输入流程KEY' }
  ],
  processGroupId: [
    { required: true, message: '请选择所属流程组' }
  ],
  formId: [
    { required: true, message: '请选择绑定表单' }
  ],
  processAdmin: [
    { required: true, message: '请选择流程管理员' }
  ]
};