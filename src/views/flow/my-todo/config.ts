import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_DETAIL: 'flow.task.detail',
  PERMISSION_COMPLETE: 'flow.task.complete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '流程名称',
    name: 'processName',
    placeholder: '配置名称',
    defaultValue: ''
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '流程编号',
    dataIndex: 'bizNumber'
  },
  {
    title: '流程名称',
    dataIndex: 'processName'
  },
  {
    title: '任务名称',
    dataIndex: 'taskName'
  },
  {
    title: '发起人',
    dataIndex: 'startUserName'
  },
  {
    title: '任务开始时间',
    dataIndex: 'createTime',
    align: 'center'
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);
/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '办理',
    operator: 'audit',
    icon: '',
    permission: [permission.PERMISSION_DETAIL]
  }
]);