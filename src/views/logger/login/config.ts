import { ref } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'logger.login.list',
  PERMISSION_DETAIL: 'logger.login.detail'
};

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '登录名',
    dataIndex: 'loginName'
  },
  {
    title: '登录IP',
    dataIndex: 'loginIp'
  },
  {
    title: '浏览器',
    dataIndex: 'browser',
    align: 'center'
  },
  {
    title: '操作系统',
    dataIndex: 'os',
    align: 'center'
  },
  {
    title: '登录结果',
    dataIndex: 'status',
    align: 'center',
    slotName: 'status'
  },
  {
    title: '提示消息',
    dataIndex: 'msg'
  },
  {
    title: '登录时间',
    dataIndex: 'loginTime',
    align: 'center'
  }
]);