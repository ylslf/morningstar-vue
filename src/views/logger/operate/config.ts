import { ref } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'logger.operate.list',
  PERMISSION_DETAIL: 'logger.operate.detail'
};

/**
 * 列操作
 */
export const columnOperators = [
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL]
  }
];

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '操作名称',
    dataIndex: 'title',
    fixed: 'left',
    width: 200
  },
  {
    title: '请求地址',
    dataIndex: 'operateUrl',
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '请求方式',
    dataIndex: 'requestMethod',
    align: 'center'
  },
  {
    title: '执行方法',
    dataIndex: 'method',
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '请求IP',
    dataIndex: 'operateIp'
  },
  {
    title: '浏览器',
    dataIndex: 'browser',
    align: 'center'
  },
  {
    title: '操作系统',
    dataIndex: 'os',
    align: 'center',
    width: 240,
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '耗时',
    dataIndex: 'costTime'
  },
  {
    title: '操作时间',
    dataIndex: 'operateTime',
    align: 'center',
    width: 200
  },
  {
    title: '提示消息',
    dataIndex: 'resultMsg',
    width: 150,
    ellipsis: true,
    tooltip: { position: 'left' }
  },
  {
    title: '响应码',
    dataIndex: 'status',
    align: 'center',
    fixed: 'right',
    width: 120
  },
  {
    title: '操作人',
    dataIndex: 'operateName',
    align: 'center',
    fixed: 'right',
    width: 120
  },
  {
    title: '操作',
    dataIndex: 'operations',
    fixed: 'right',
    width: 120,
    slotName: 'operations',
    align: 'center'
  }
]);