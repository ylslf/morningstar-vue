import { ref, reactive } from 'vue';
import { TableColumnData } from '@arco-design/web-vue/es/table/interface';

export const permission = {
  PERMISSION_LIST: 'system.form.list',
  PERMISSION_DETAIL: 'system.form.detail',
  PERMISSION_UPDATE: 'system.form.update',
  PERMISSION_ADD: 'system.form.add',
  PERMISSION_DELETE: 'system.form.delete'
};

/**
 * header query config
 * 表格查询头配置
 */
export const queryConfig = reactive([
  {
    type: 'input',
    label: '表单名称',
    name: 'formName',
    placeholder: '表单名称',
    defaultValue: ''
  }
]);

/**
 * 表格头操作表
 */
export const headerOperators = reactive([
  {
    name: 'table.operator.add',
    operator: 'add',
    icon: 'icon-plus',
    permission: permission.PERMISSION_ADD
  }
]);

/**
 * 列操作
 */
export const columnOperators = reactive([
  {
    name: '设计表单',
    operator: 'design',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_UPDATE]
  },
  {
    name: '表单权限',
    operator: 'permission',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_UPDATE]
  },
  {
    name: '预览表单',
    operator: 'preview',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL]
  },
  {
    name: '详细',
    operator: 'detail',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DETAIL, permission.PERMISSION_UPDATE]
  },
  {
    name: '删除',
    operator: 'delete',
    icon: 'icon-plus',
    permission: [permission.PERMISSION_DELETE]
  }
]);

/**
 * 表列配置
 */
export const tableColumns = ref<TableColumnData[]>([
  {
    title: '表单名称',
    dataIndex: 'formName'
  },
  {
    title: '是否设计',
    dataIndex: 'formConfig',
    slotName: 'formConfig',
    align: 'center'
  },
  {
    title: '状态',
    dataIndex: 'status',
    slotName: 'status',
    align: 'center'
  },
  {
    title: '备注',
    dataIndex: 'remark',
    ellipsis: true,
    tooltip: true
  },
  {
    title: '操作',
    dataIndex: 'operations',
    slotName: 'operations',
    align: 'center'
  }
]);